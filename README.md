**This is a parallel GPU Molecular dynamics code of Lennard-Jones fluid model with
 DPD thermostat, written in CUDA.**

    	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, first install recommended nvidia driver and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile  using: 
 *      nvcc filename.cu -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w




      
      -arch=sm_61 is the flag used for the GPU card architecture. 
      -arch=sm_61    is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on
     Tesla V100, change -arch=sm_61 to -arch=sm_70.



 *	 Run :
 *		 nohup ./a.out &
 	 			



     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020
