/* 
 *	This is a parallel GPU Molecular dynamics code of Lennard Jones fluid with DPD thermostat, written in CUDA.
 *   
 *	Rajneesh Kumar
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	12 Dec, 2020
 */



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <curand_kernel.h>
#include <time.h>
#include "ran2.h"


typedef struct atomdesc {
	double px, py, pz; // positions
	double vx, vy, vz; // velocities
	double ax, ay, az; // accelerations 
	double fx, fy, fz; // forces 
} Mol;

// Global declaration of Variables
Mol * mol;       /* List of all data points */
long seed;      // initial seed value to generate random number through ran2 (long *idum) function


// This function generate initial seed for random number generator for each thread
__global__ 
void setup_kernel (curandState *state, int nMol)
{
	int id = threadIdx.x + blockIdx.x *blockDim.x;
	// Each thread gets same seed, a different sequence number, no offset
	if (id < nMol) curand_init(1234, id, 0, &state[id]);
}


//  Verlet-Neighbor list calculation
__global__
void getNeibrList(Mol * mol, int nMol, double length, double rCut, 
				  double verSkin, double Sigma, int * StartingPoint,
				  double * DisplacementsX, double * DisplacementsY, double * DisplacementsZ,
				  double gammaStr, double gammaDot, int * nbrCount, int * nbrList)
{
	int j,k;
	double dx,dy,dz,cx,cy,rij;
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < (nMol)) {
		nbrCount[i] = 0;
		
		
		k = i*nMol;
		StartingPoint[i] = k;
		//printf("%d\t %d\n", StartingPoint[i], k);
		for(j=0; j < nMol; j++){
			//if(j == i) rij = length;
			if(j != i){
				dx = mol[i].px - mol[j].px;
				dy = mol[i].py - mol[j].py;
				dz = mol[i].pz - mol[j].pz;
				
				cy = round(dy / length);
				cx = dx - cy * length * gammaStr;
				
				dx = cx - round(cx / length) * length;
				dy = dy - cy * length;
				dz = dz - round(dz / length) * length;
				
				rij = sqrt(dx * dx + dy * dy + dz * dz);
				
				
				if( rij < (rCut + verSkin)*Sigma) {
					nbrCount[i] += 1;
					nbrList[k] = j;
					k++;
				}
			}
		}
		
		DisplacementsX[i] = 0.0;
		DisplacementsY[i] = 0.0;
		DisplacementsZ[i] = 0.0;
		
	}	
}


/* ################################################################################################################*/
//  This kernel will calculate forces and total potential energy for the Lennard-Jonnes potential ***********************
__global__
void CalcForce(curandState *state, Mol * mol, double MDdeltaT, int nMol,double length,double rCut, double Sigma, double Epsilon, double * PE, double gammaStr, int * StartingPoint, int * nbrCount, int * nbrList, double sigmaD, double fricCoeff)
{
	int i = threadIdx.x + blockDim.x * blockIdx.x; /* Get the indices of the atom */
	
	
	double dx,dy,dz,dvx,dvy,dvz,rsq,r,fx,fy,fz,rri,rri3,ene,cx,cy,rgx,rgy,rgz,fDx,fDy,fDz,fRx,fRy,fRz,rvDotProd,wD,wR;
	
	if(i == 0)    PE[0] = 0.0;      // Initialize the potential energy variable
	__syncthreads();                // Wait untill all the threads have reached this barrier
	
	if (i < (nMol-1))                    // Total number of thread can be equal to or more than number of atoms. Discard execution for ghost theads
	{
		// initialize force for each atom
		//		mol[i].fx = 0.0;
		//		mol[i].fy = 0.0;
		//		mol[i].fz = 0.0;
		/* ##################################*/
		
		// Copy state to local memory for efficiency
		curandState localState = state[i];
		
		// loop through particles
		for (int k = StartingPoint[i]; k < (StartingPoint[i]+nbrCount[i]); k++) {
			int j = nbrList[k];
			
			if (j > i) {
				// Calculate distance between particle i and j in x, y, z direction	
				dx = mol[i].px - mol[j].px;
				dy = mol[i].py - mol[j].py;
				dz = mol[i].pz - mol[j].pz;
				/* #################################################################*/
				// Lees-Edwards boundary condition in x,y,z direction
				cy = round(dy / length);
				cx = dx - cy * length * gammaStr;
				dx = cx - round(cx / length) * length;
				dy -= length * round(dy / length);
				dz -= length * round(dz / length);
				/* ##################################*/
				rsq = dx*dx + dy*dy + dz*dz;             // r^2 = x^2 + y^2 + z^2
				r = sqrt(rsq);                           // r = sqrt(r^2)
				
				if (r < (rCut * Sigma)) {               // Interaction Cutoff radius condition. Above this cutoff, potential is zero
					rri = 1.0 / rsq;                //1/r^2
					rri3 = rri * rri * rri;         //1/r^6
					// Calculate LJ potential U(r)=4 epsilon ((sigma/r)^12 - (sigma/r)^6)
					ene = 4.0 * Epsilon *  rri3 * ((rri3 * pow (Sigma, 12)) - (pow (Sigma, 6)));
					// force x,y,z components calculations
					fx = 48.0 * dx * Epsilon * ((rri3 * ((rri3 * pow (Sigma, 12)) - (0.5 * pow (Sigma, 6))) * rri));
					fy = 48.0 * dy * Epsilon * ((rri3 * ((rri3 * pow (Sigma, 12)) - (0.5 * pow (Sigma, 6))) * rri));
					fz = 48.0 * dz * Epsilon * ((rri3 * ((rri3 * pow (Sigma, 12)) - (0.5 * pow (Sigma, 6))) * rri));
					/* #############################################################################################*/
					atomicAdd(&(mol[i].fx), fx);
					atomicAdd(&(mol[i].fy), fy);
					atomicAdd(&(mol[i].fz), fz);
					
					atomicAdd(&(mol[j].fx), -fx);
					atomicAdd(&(mol[j].fy), -fy);
					atomicAdd(&(mol[j].fz), -fz);
					atomicAdd(&(PE[0]), ene); // add potential energy of each atom. atomicAdd performs an atomic addition of data to the contents
					// of memory and ensures that no to thread can write to that memory at the same time.
					
					
					//relative velocity and its dot prodect with displacement vector
					dvx = mol[i].vx - mol[j].vx;
					dvy = mol[i].vy - mol[j].vy;
					dvz = mol[i].vz - mol[j].vz;
					
					
					rvDotProd = dx*dvx + dy*dvy + dz*dvz;
					// F^D---Dissipitive force calculation
					wD = (1.0 - (sqrt(rsq) / rCut)) * (1.0 - (sqrt(rsq) / rCut));
					wR = sqrt(wD);
					fDx = -wD * fricCoeff * rvDotProd * rri * dx;
					fDy = -wD * fricCoeff * rvDotProd * rri * dy;
					fDz = -wD * fricCoeff * rvDotProd * rri * dz;
					atomicAdd(&(mol[i].fx), fDx);
					atomicAdd(&(mol[i].fy), fDy);
					atomicAdd(&(mol[i].fz), fDz);
					
					atomicAdd(&(mol[j].fx), -fDx);
					atomicAdd(&(mol[j].fy), -fDy);
					atomicAdd(&(mol[j].fz), -fDz);
					
					// Generate gaussian-random with mean 0 and variance 1
					rgx = curand_normal (&localState);
					rgy = curand_normal (&localState);
					rgz = curand_normal (&localState);
					
					// F^R---random force calculation
					fRx = ((wR * sigmaD) / (sqrt(MDdeltaT) * sqrt(rsq))) * rgx * dx;
					fRy = ((wR * sigmaD) / (sqrt(MDdeltaT) * sqrt(rsq))) * rgy * dy;
					fRz = ((wR * sigmaD) / (sqrt(MDdeltaT) * sqrt(rsq))) * rgz * dz;
					
					atomicAdd(&(mol[i].fx), fRx);
					atomicAdd(&(mol[i].fy), fRy);
					atomicAdd(&(mol[i].fz), fRz);
					
					atomicAdd(&(mol[j].fx), -fRx);
					atomicAdd(&(mol[j].fy), -fRy);
					atomicAdd(&(mol[j].fz), -fRz);
					
				}		
			}
		}
		// Copy state back to global memory
		state[i] = localState;
	}	
}


/* ##########################################################################################################################################*/
//  This kenel will integrate the equation of motion using Velocity Verlet and calculate kinetic energy ***********************
__global__
void Update(Mol * mol, double MDdeltaT, int nMol, double length, double rCut, double * KE, double gammaStr, double gammaDot, double verSkin, int * StartingPoint, int * nbrCount, int * nbrList, double * DisplacementsX, double * DisplacementsY, double * DisplacementsZ, double Sigma) 
{
	int i = threadIdx.x + blockDim.x * blockIdx.x; /* Get the indices of the atom */
	double old_ax,old_ay,old_az, old_px, old_py, old_pz, ene, cx, cy, dx, dy, dz;
	if(i == 0)      KE[0] = 0.0;      // Initialize the kinetic energy variable
	__syncthreads();                // Wait untill all the threads have reached this barrier
	if (i < nMol)                   // Total number of thread can be equal to or more than number of atoms. Discard execution for ghost theads
	{       
		old_px = mol[i].px;
		old_py = mol[i].py;
		old_pz = mol[i].pz;
		// store previous step's acceleration
		old_ax = mol[i].ax;
		old_ay = mol[i].ay;
		old_az = mol[i].az;
		/* #########################*/
		
		// Calculate  acceleration from force, a=F/m: here m=1
		mol[i].ax = mol[i].fx;
		mol[i].ay = mol[i].fy;
		mol[i].az = mol[i].fz;
		/* #########################*/
		
		// velocity update
		mol[i].vx = mol[i].vx + 0.5*(mol[i].ax + old_ax)*MDdeltaT- gammaDot * mol[i].vy * MDdeltaT;
		mol[i].vy = mol[i].vy + 0.5*(mol[i].ay + old_ay)*MDdeltaT;
		mol[i].vz = mol[i].vz + 0.5*(mol[i].az + old_az)*MDdeltaT;
		/* ##################################################################*/
		
		ene = 0.5 * (mol[i].vx * mol[i].vx + mol[i].vy * mol[i].vy + mol[i].vz * mol[i].vz);
		atomicAdd(&(KE[0]), ene); // add kinetic energy of each atom. atomicAdd performs an atomic addition of 
		//data to the contents of memory and ensures that no to thread can write to that memory at the same time.
		
		// integrate to get position
		mol[i].px = mol[i].px + mol[i].vx * MDdeltaT + 0.5 * mol[i].ax * MDdeltaT *MDdeltaT + gammaDot * mol[i].py * MDdeltaT;
		mol[i].py = mol[i].py + mol[i].vy * MDdeltaT + 0.5 * mol[i].ay * MDdeltaT *MDdeltaT;
		mol[i].pz = mol[i].pz + mol[i].vz * MDdeltaT + 0.5 * mol[i].az * MDdeltaT *MDdeltaT;
		/* #####################################################################################*/
		
		
		dx = mol[i].px - old_px;
		dy = mol[i].py - old_py;
		dz = mol[i].pz - old_pz;
		
		DisplacementsX[i] += dx;
		DisplacementsY[i] += dy;
		DisplacementsZ[i] += dz;
		mol[i].fx = 0.0;
		mol[i].fy = 0.0;
		mol[i].fz = 0.0;
		
		// Lees-Edwards Periodic boundary condition in x,y,z direction
		//cy = floor(mol[i].py / length) * length * gammaStr;
		//cx = mol[i].px - cy;
		
		//mol[i].px = cx - floor(cx / length) * length;
		//mol[i].py -= length * floor(mol[i].py / length);
		//mol[i].pz -= length * floor(mol[i].pz / length);
		/* ######################################################*/
		
		
		
	}
}


/* ############################################################################################################################*/
void MDStep(Mol * mol, int nMol, double MDdeltaT, double stepLimit, double length, double rCut, double Sigma, double Epsilon, double gammaDot, double gammaMax, double temperature) { 
	
	FILE *fp;
	fp = fopen ("LJMolPropGPU_AtomicOpertnDPDN1000.dat", "w");   //open file to dump the results
	
	int threads_per_block,i, vrFlag = 0;                  /* Number of threads per block */
	unsigned int block_count;               /* Number of blocks per grid */
	int t;
	double timeNow, drmag, fricCoeff, sigmaD;
	size_t size, size1, size2, size3, size4, size5;
	curandState *devStates;
	
	size = nMol * sizeof(Mol); 
	size1 = sizeof(double);
	size2 = nMol * sizeof(int);
	size3 = nMol * nMol * sizeof(int);
	size4 = nMol * sizeof(double);
	size5 = nMol * sizeof(curandState);
	double verSkin;
	verSkin = 0.5;
	fricCoeff = 1.0;
	sigmaD = sqrt(2.0 * fricCoeff * temperature);
	// declare device pointer
	Mol *d_mol;    
	double *KE, *PE, gammaStr;
	double *d_KE, *d_PE;
	
	int *nbrCount, *nbrList, *StartingPoint;
	double *DisplacementsX, *DisplacementsY, *DisplacementsZ;
	
	int *d_nbrCount, *d_nbrList, *d_StartingPoint;
	double *d_DisplacementsX, *d_DisplacementsY, *d_DisplacementsZ;
	
	/* #########################*/
	
	/* Allocate pointers in host memory */
	KE = (double *)malloc(size1);
	PE = (double *)malloc(size1);
	nbrCount = (int *)malloc(size2);
	nbrList = (int *)malloc(size3);
	StartingPoint = (int *)malloc(size2);
	DisplacementsX = (double *)malloc(size4);
	DisplacementsY = (double *)malloc(size4);
	DisplacementsZ = (double *)malloc(size4);
	/* #################################*/
	
	/* Allocate pointers in device memory */
	cudaMalloc((void**) &d_mol, size);
	cudaMalloc((void**)&d_KE, size1);
	cudaMalloc((void**)&d_PE, size1);
	cudaMalloc((void**)&d_nbrCount, size2);
	cudaMalloc((void**)&d_nbrList, size3);
	cudaMalloc((void**)&d_StartingPoint, size2);
	cudaMalloc((void**)&d_DisplacementsX, size4);
	cudaMalloc((void**)&d_DisplacementsY, size4);
	cudaMalloc((void**)&d_DisplacementsZ, size4);
	cudaMalloc ((void **)&devStates, size5);
	
	/* ###################################*/
	
	/* Copy data from host memory to device memory */       
	cudaMemcpy(d_mol, mol, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_KE, KE, size1, cudaMemcpyHostToDevice);
	cudaMemcpy(d_PE, PE, size1, cudaMemcpyHostToDevice);
	cudaMemcpy(d_nbrCount, nbrCount, size2, cudaMemcpyHostToDevice);
	cudaMemcpy(d_nbrList, nbrList, size3, cudaMemcpyHostToDevice);
	cudaMemcpy(d_StartingPoint, StartingPoint, size2, cudaMemcpyHostToDevice);
	cudaMemcpy(d_DisplacementsX, DisplacementsX, size4, cudaMemcpyHostToDevice);
	cudaMemcpy(d_DisplacementsY, DisplacementsY, size4, cudaMemcpyHostToDevice);
	cudaMemcpy(d_DisplacementsZ, DisplacementsZ, size4, cudaMemcpyHostToDevice);
	
	/* ################################################*/
	
	/* Define block size */
	threads_per_block =32;
	/* Calculate number of blocks.  If we just computed n/threads_per_block */
	/* we might get fewer threads than vector components.                   */
	block_count = (nMol + threads_per_block - 1)/threads_per_block;
	/* #####################################################################*/
	
	// Setup prng states
	setup_kernel <<<block_count, threads_per_block>>>(devStates, nMol);
	
	gammaStr = 0;
	getNeibrList<<< block_count, threads_per_block >>>(d_mol, nMol, length, rCut, verSkin, Sigma, d_StartingPoint, d_DisplacementsX, d_DisplacementsY, d_DisplacementsZ, gammaStr, gammaDot, d_nbrCount, d_nbrList);
	
	for (t=1; t <= stepLimit; t++) {        // Loop over time
		timeNow = t * MDdeltaT;
		
		// uniform shear
		gammaStr = gammaMax * timeNow;
		
		/* Launch Kernel                     */
		Update<<< block_count, threads_per_block >>>( d_mol, MDdeltaT, nMol,length, rCut, d_KE, gammaStr, gammaDot, verSkin, d_StartingPoint, d_nbrCount, d_nbrList, d_DisplacementsX, d_DisplacementsY, d_DisplacementsZ, Sigma);
		CalcForce<<< block_count, threads_per_block >>>(devStates, d_mol, MDdeltaT, nMol,length, rCut, Sigma, Epsilon, d_PE, gammaStr, d_StartingPoint, d_nbrCount, d_nbrList, sigmaD, fricCoeff);
		
		cudaMemcpy(DisplacementsX, d_DisplacementsX, size4, cudaMemcpyDeviceToHost);
		cudaMemcpy(DisplacementsY, d_DisplacementsY, size4, cudaMemcpyDeviceToHost);
		cudaMemcpy(DisplacementsZ, d_DisplacementsZ, size4, cudaMemcpyDeviceToHost);
		
		for(i=0; i<nMol; i++){
			drmag = DisplacementsX[i] * DisplacementsX[i] + DisplacementsY[i] * DisplacementsY[i] + 
			DisplacementsZ[i] * DisplacementsZ[i];
			drmag = sqrt(drmag);
			if(drmag > verSkin) vrFlag++;
		}
		if(vrFlag > 0){
			getNeibrList<<< block_count, threads_per_block >>>(d_mol, nMol, length, rCut, verSkin, Sigma, d_StartingPoint, d_DisplacementsX, d_DisplacementsY, d_DisplacementsZ, gammaStr, gammaDot, d_nbrCount, d_nbrList);
			vrFlag = 0;
		}
		/* #######################################################################################################*/
		
		if ((t-1)%10 == 0) {
			//cudaMemcpy(mol, d_mol, size, cudaMemcpyDeviceToHost);
			cudaMemcpy(KE, d_KE, size1, cudaMemcpyDeviceToHost);/* Copy result from device memory to host memory */
			cudaMemcpy(PE, d_PE, size1, cudaMemcpyDeviceToHost);/* Copy result from device memory to host memory */
			fprintf (fp, "%lf\t %lf\t %lf\t %lf\n", timeNow,KE[0] / (nMol), PE[0] / (nMol), (2.0 * KE[0]) / (3.0 *(nMol-1.0))); // dump results to the file
			fflush (fp);
			
		}
	}
	fclose (fp);            // close file
	cudaFree(d_mol);        // free d_mol
}


//#########################################
void InitCoords (Mol * mol, int nMol, double Sigma, double Epsilon, double length)
{
	int n, nx, ny, nz, i;
	
	//**********************intialize Particle Position***********************
	
	nx=0;ny=0;nz=0;
	n = 2;
	while ((n*n*n)<nMol) n++;
	for (i=0; i < nMol; i++) { 
		mol[i].px = ((double)nx+0.5) * length / n;
		mol[i].py = ((double)ny+0.5) * length / n;
		mol[i].pz = ((double)nz+0.5) * length / n;
		nx++;
		if (nx == n) {
			nx = 0;
			ny++;
			if (ny == n) {
				ny = 0;
				nz++;
			}
		}
	}
}


/* Gaussian random number generator with mean 0 and variance 1
 *   Acknowledgement Numerical Recipies in C  */
double gasdev(long *idum)
{
	//double ran2(long *idum);
	static int iset=0;
	static double gset;
	double fac,rsq,v1,v2;
	if (*idum < 0) iset=0;
	if (iset == 0) {
		do {
			v1=2.0*ran2(idum)-1.0;
			v2=2.0*ran2(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return (double) v2*fac;
	} else {
		iset=0;
		return (double) gset;
	}
}


//################# initialize velocities########################
void InitVels (Mol * mol, int nMol)
{
	int n;
	double ke, velMag, vSumx, vSumy, vSumz, tempInitial;
	tempInitial = 10.0;     //Initial temperature
	ke = 0.0;              //Initialize kinetic energy
	vSumx =0.0; vSumy = 0.0; vSumz = 0.0; 
	for (n = 0; n < nMol; n ++) {
		mol[n].vx = gasdev(&seed);      //Generate velocities using gaussian random number with mean 0 and unit variance
		mol[n].vy = gasdev(&seed);      //Generate velocities using gaussian random number with mean 0 and unit variance
		mol[n].vz = gasdev(&seed);      //Generate velocities using gaussian random number with mean 0 and unit variance
		
		vSumx += mol[n].vx;     // sum velocity of x component
		vSumy += mol[n].vy;     // sum velocity of y component
		vSumz += mol[n].vz;     // sum velocity of z component
	}
	
	for (n = 0; n < nMol; n ++) {
		mol[n].vx += (- 1.0 / nMol) * vSumx;
		mol[n].vy += (- 1.0 / nMol) * vSumy;
		mol[n].vz += (- 1.0 / nMol) * vSumz;
		ke=ke+0.5 * ((mol[n].vx * mol[n].vx) + (mol[n].vy * mol[n].vy)  + (mol[n].vz * mol[n].vz)) ;    // Calculate kinetic energy
	}
	if (tempInitial > 0.) {
		velMag = sqrt(3.0 * (nMol) * tempInitial / (2.0 * ke)); // Scale factor to the velocity 
		
		for (n = 0; n < nMol; n ++) {
			mol[n].vx *= velMag;    // scale velocity x component
			mol[n].vy *= velMag;    // scale velocity y component
			mol[n].vz *= velMag;    // scale velocity z component
		}
	}
}



//################ initialize accelerations and forces #########################
void InitAccels (Mol * mol, int nMol)
{
	int n;
	for (n = 0; n < nMol; n ++) {
		mol[n].ax = 0.0;
		mol[n].ay = 0.0;
		mol[n].az = 0.0;
		mol[n].fx = 0.0;
		mol[n].fy = 0.0;
		mol[n].fz = 0.0;
	}
}



////************************** MAIN ***********************************
int main(int argc, char **argv)
{
	
	int nMol, stepLimit;
	double length, rho, Sigma, Epsilon, MDdeltaT, rCut, gammaMax, gammaDot, temperature;
	
	nMol = 1000;                              // Number of paricles....
	rho = 1.2;                                // density
	length = pow (nMol/rho,0.333333333);      // length of simulation box
	seed = -98765432;                         // Initialize seed
	rCut = 2.5;                               // cutoff of the potential
	stepLimit = 10000;                          // Number of total MD time steps....
	MDdeltaT = 0.002;                         // MD delta_t
	Sigma = 1.00;                             // diameter od sphere
	Epsilon = 1.00;                           // Interaction depth of the potential
	gammaMax = 0.0;
	gammaDot = gammaMax;
	temperature = 0.6;
	
	
	mol = (Mol *)malloc(sizeof(Mol)*nMol);          // Allocate structure 'mol' with the type 'Mol'
	
	InitCoords (mol, nMol, Sigma, Epsilon, length); //initialize coordinates
	InitVels (mol, nMol);                           //initialize velocity
	InitAccels (mol, nMol);                         //initialize acceleration
	
	MDStep(mol, nMol, MDdeltaT, stepLimit, length, rCut, Sigma, Epsilon, gammaDot, gammaMax, temperature); 	// call to Molecular dynamics
	return 0;
}
